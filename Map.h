#pragma once
#include "Engine/GameObject.h"

//タイトルシーン管理するクラス
class Map : public GameObject
{
public:
	//フィールドの種類
	enum Type
	{
		TYPE_GRS,   //草
		TYPE_SAD,   //砂
		TYPE_WTR,   //水
		TYPE_WAL,   //壁
		TYPE_SDWL,  //砂壁
		TYPE_MAX
	};

	//移動用ベクトル
	XMVECTOR move = g_XMZero;


public:
	//探索範囲
	int Search_[4][2] =
	{
		{1,0},   //上
		{0,-1},  //左
		{-1,0},  //下
		{0,1}    //右
	};

	//方向
	enum DIR {
		UP = 0,   //上
		LEFT,     //左
		DOWN,     //下
		RIGHT,    //右
		DIR_MAX
	}Dir_ = UP;

	//マップの縦と横
	static const int max_width = 29;
	static const int max_height = 29;

private:
	//配列に格納されている情報を受け取る変数
	int Type_;
	//モデル番号
	int GroundModel_[TYPE_MAX];
	
	//同じモデルを生成しないための確認用配列
	int MapParam_[3] = {1,1,1};
	//ランダム変数
	int Rand_;

public:

	//壁ありの配列
	int Map_[max_height][max_width];
	////壁なしの配列
	int TerrainMap_[max_height][max_width];
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Map(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//地形を生成(初期設定以外の地形を生成)
	//引数　地形を生成する位置を決める判定,まだ生成していない地形
	void Mapparameter(BOOLEAN branch,int num);
	

	//壁生成
	void Create();

	//------------------------ポインタで他から呼ばれる関数(引数：zはZ座標、xはX座標)----------------------------------
	//当たり判定
	//引数　z=Z座標,x=X座標
	//戻り値　false 又は
	bool IsWall(float z, float x) { if (Map_[(int)z][(int)x] >= 3)return false; return true; };
	//砂壁かどうか
	//引数　z=Z座標,x=X座標
	bool IsSandWall(float z, float x) { if (Map_[(int)z][(int)x] == 4)return true; return false; }
	//引数で渡された配列の中の値を返す
	//引数　z=Z座標,x=X座標
	//戻り値　速度
	float GetMap(float z, float x); 
	//方向
	//引数　向き情報
	//戻り値　向く方向(DIR型)
	XMVECTOR Dir(DIR dir);
	//前回と今回の向きを比較
	//引数　向く方向(Enemyの方で数値で管理しているためint型)
	//戻り値　true 又は　false
	bool IsDir(int isdir);
	//進行方向をセット
	void SetDir(int direction);

};