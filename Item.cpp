#include "Item.h"
#include "Engine/Direct3D.h"
#include "Engine/Image.h"
#include "Engine/Input.h"

//コンストラクタ
Item::Item(GameObject* parent)
	: GameObject(parent, "Item"),HpGage_(-1)
{
	for (int i = 0; i < ItemKinds_; i++)
	{
		WeaponIcon_[i] = -1;
	}
	for (int i = 0; i < NumberKinds_; i++)
	{
		WeaponIcon_[i] = -1;
	}
}

//初期化
void Item::Initialize()
{
	//アイテム保持配列の初期化
	memset(HaveWeapon_, 0, ItemKinds_ * sizeof(HaveWeapon_[0]));

	//読み込むファイル名の設定
	//アイテム
	static const std::string ItemFileName[ItemKinds_]
	{
		"Image/pickel.png",
		"Image/sword.png",
		"Image/lance.png"
	};

	//数字
	static const std::string NumberFileName[NumberKinds_]
	{
		"Image/0.png",
		"Image/1.png",
		"Image/2.png",
		"Image/3.png",
		"Image/4.png",
		"Image/5.png"
	};
	


	//画像データ分の読み込みと描画位置設定
	//アイテム
	for (int i = 0; i < (sizeof(ItemFileName) / sizeof(*ItemFileName)); i++)
	{
		WeaponIcon_[i] = Image::Load(ItemFileName[i].c_str());
		assert(WeaponIcon_[i] >= 0);
		wPosition_[i].position_ = { 0.25f * i - 0.25f,-0.9f,0,0 };
		nPosition_[i].position_ = { 0.25f * i - 0.2f,-0.95,0.0 };
		wPosition_[i].scale_ = { 0.85,0.85,0,0 };

	}

	//数字
	for (int i = 0; i < (sizeof(NumberFileName) / sizeof(*NumberFileName)); i++)
	{
		NumberIcon_[i] = Image::Load(NumberFileName[i].c_str());
		assert(WeaponIcon_[i] >= 0);
	}

	//HP
	HpGage_ = Image::Load("Image/HP.png");
	assert(HpGage_ >= 0);

	transform_.position_ = { 0,0.95f,0,0 };
	transform_.scale_ = { 2.0f,2.0f,0,1 };
}

//更新
void Item::Update()
{
	//→が押されたら
	if (Input::IsKeyDown(DIK_RIGHT))
	{
		//カーソル位置が1番右でなければカーソルを右に１つずらす
		if (Cursor_ < ItemKinds_)Cursor_ += 1;
	}
	//←が押されたら
	if (Input::IsKeyDown(DIK_LEFT))
	{
		//カーソル位置が1番左でなければカーソルを左に１つずらす
		if (Cursor_ > 0)Cursor_ -= 1;
	}
}

//描画
void Item::Draw()
{
	for (int i = 0; i < ItemKinds_; i++)
	{
		//アイテムの表示位置
		Image::SetTransform(WeaponIcon_[i], wPosition_[i]);
		//数字の表示位置(アイテム画像の右下)
		Image::SetTransform(NumberIcon_[HaveWeapon_[i]], nPosition_[i]);
		//選択しているアイテム以外透明
		if (i != Cursor_)	Image::SetAlpha(WeaponIcon_[i], 100);
		else Image::SetAlpha(WeaponIcon_[i], 255);
		Image::Draw(WeaponIcon_[i]);
		Image::Draw(NumberIcon_[HaveWeapon_[i]]);
	}

	Image::SetTransform(HpGage_, transform_);
	Image::Draw(HpGage_);
}

//開放
void Item::Release()
{
}


//アイテムを入手
void Item::GetItem()
{
	int random = rand() % 30;
	//選ばれたアイテムが5個未満なら
	if(HaveWeapon_[random%3] < 5)
	//所持数を増やす
	HaveWeapon_[random%3]++;
}

bool Item::UseItem()
{
	//カーソルで選択しているアイテムを使用
	if (HaveWeapon_[Cursor_] > 0)
	{
		//所持数を１つ減らす
		HaveWeapon_[Cursor_] -= 1;
		//武器を使って倒せるためtrueを返す
		return true;
	}
	//そのアイテムは持ってなく、敵を倒せないのでfalseを返す
	else return false;
}

int Item::DamageCalculation(int Hp)
{
	return Hp - WeaponAttack_[Cursor_];
}



