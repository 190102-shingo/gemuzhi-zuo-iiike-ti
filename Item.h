#pragma once
#include "Engine/GameObject.h"

//タイトルシーン管理するクラス
class Item : public GameObject
{
private:
	//武器種類数
	static const int ItemKinds_ = 3;
	//数字アイコン
	static const int NumberKinds_ = 6;
	//画像を読み込む変数
	int WeaponIcon_[ItemKinds_];
	int NumberIcon_[NumberKinds_];
	int HpGage_;
public:
	//アイテムの種類
	enum Weapon
	{
		PICKAXE,  //つるはし
		SWORD,    //剣
		LANCE,    //槍
		MAX_WEAPON
	};

	//保持している個数
	int HaveWeapon_[ItemKinds_]; 
	//各アイテムの攻撃力
	const int WeaponAttack_[ItemKinds_] = {15,20,18};
	//アイテムのカーソル
	int Cursor_ = 0;

	Transform wPosition_[ItemKinds_];
	Transform nPosition_[NumberKinds_];

public:	
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Item(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//アイテム入手
	void GetItem();

	//アイテム使用
	bool UseItem();

	//ダメージ計算
	//引数：対象のHP
	//戻り値：ダメージ計算後のHP
	int DamageCalculation(int Hp);

};