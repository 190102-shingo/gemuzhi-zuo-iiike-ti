#include "Map.h"
#include "Engine/Model.h"
#include "Engine/Input.h"

//コンストラクタ
Map::Map(GameObject* parent)
	: GameObject(parent, "Map")
{

	//モデルを入れる変数配列の初期化
	for (int i = 0; i < 4; i++)
	{
		GroundModel_[i] = -1;
	}

	

	Rand_ = rand() % 3;
	//地面配列の初期化
	//マップの真ん中に表示される
	for (int z = 0; z < max_height; z++)
	{
		for (int x = 0; x < max_width; x++)
		{
			Map_[z][x] = Rand_;
			TerrainMap_[z][x] = Rand_;
		}
	}
	MapParam_[Rand_] = 0;

	Rand_ = (rand() % 10) % 2;
	//マップの右上と左下に表示される
	for (int i = 0; i < 3; i++)
	{
		if (MapParam_[i] != 0)
		{
			Mapparameter(Rand_,i);
			Rand_ = !Rand_;
		}
	}
	
	
}

//初期化
void Map::Initialize()
{
	Create();

	//読み込むファイル名の設定
	static const std::string ModelFileName_[5]
	{
		"Map/green.fbx",
		"Map/sand.fbx",
		"Map/sea.fbx",
		"Map/wall.fbx",
		"Map/sand_wall.fbx"
	};

	//モデルデータ分の読み込み
	for (int i = 0; i < (sizeof(ModelFileName_) / sizeof(*ModelFileName_)); i++)
	{
		GroundModel_[i] = Model::Load(ModelFileName_[i].c_str());
		assert(GroundModel_[i] >= 0);
	}

	
}

//更新
void Map::Update()
{
	
}

//描画
void Map::Draw()
{
	for (int z = 0; z < max_height; z++)
	{
		transform_.position_.vecZ = z + 0.5;
		for (int x = 0; x < max_width; x++)
		{
			transform_.position_.vecX = x + 0.5;
			
			Type_ = Map_[z][x];
			Model::SetTransform(GroundModel_[Type_], transform_);
			Model::Draw(GroundModel_[Type_]);
			
		}
		
	}
}

//開放
void Map::Release()
{
}

//マップ生成
void Map::Create()
{
	int x;//地面配列の横の要素番号
	int z;//地面配列の縦の要素番号
	int r;//ランダム値

	//壁の生成
	for (x = 0; x < max_width; x++)
	{
		Map_[0][x] = 3;
		Map_[max_height - 1][x] = 3;
	}

	for (z = 0; z < max_height; z++)
	{
		Map_[z][0] = 3;
		Map_[z][max_width - 1] = 3;
	}

	z = 2;//1行目
	for (x = 2; x < max_width - 1; x = x + 2)
	{
		r = (rand() % 12) + 1;//乱数生成(1からスタートするため+1)
		Map_[z][x] = 3;
		if (r >= 1 && r <= 3) //rが1から3のとき
		{
			if (Map_[z - 1][x] <= 2) //上に棒（壁）がなければ
			{
				Map_[z - 1][x] = 3; //上に棒を倒す。
			}
			else if (Map_[z - 1][x] == 3) //上に棒（壁）があれば
			{
				x = x - 2; //棒を倒さずに、乱数生成をやり直す。
			}
		}
		if (r >= 4 && r <= 6) //rが4から6のとき
		{
			if (Map_[z - 1][x] <= 2) //下に棒（壁）がなければ
			{
				Map_[z + 1][x] = 3; //下に棒を倒す。
			}
			else if (Map_[z + 1][x] == 3) //下に棒（壁）があれば
			{
				x = x - 2; //棒を倒さずに、乱数生成をやり直す。
			}
		}
		if (r >= 7 && r <= 9) //rが7から9のとき
		{
			if (Map_[z - 1][x] <= 2) //左に棒（壁）がなければ
			{
				Map_[z][x - 1] = 3; //左に棒を倒す。
			}
			else if (Map_[z][x - 1] == 3) //左に棒（壁）があれば
			{
				x = x - 2; //棒を倒さずに、乱数生成をやり直す。
			}
		}
		if (r >= 10 && r <= 12) //rが10から12のとき
		{
			if (Map_[z][x + 1] <= 2) //右に棒（壁）がなければ
			{
				Map_[z][x + 1] = 3; //右に棒を倒す。
			}
			else if (Map_[z][x + 1] == 3) //右に棒（壁）があれば
			{
				x = x - 2; //棒を倒さずに、乱数生成をやり直す。
			}
		}
	}

	//棒倒し法を使った壁（1）の生成（2行め以降）
	for (z = 4; z < max_height - 1; z = z + 2) //zの要素番号4から要素番号max_z-1まで、1マス飛ばしで棒倒し。
	{
		for (x = 2; x < max_width - 1; x = x + 2) //xの要素番号2から要素番号max_x-1まで、1マス飛ばしで棒倒し。
		{
			r = (rand() % 12) + 1; //乱数生成（r = 1から12のランダムな値）
			Map_[z][x] = 3; //中心から……
			if (r >= 1 && r <= 4) //rが1から4のとき
			{
				if (Map_[z + 1][x] <= 2) //下に棒（壁）がなければ
				{
					Map_[z + 1][x] = 3; //下に棒を倒す。
				}
				else if (Map_[z + 1][x] == 3) //下に棒（壁）があれば
				{
					x = x - 2; //棒を倒さずに、乱数生成をやり直す。
				}
			}
			if (r >= 5 && r <= 8) //rが5から8のとき
			{
				if (Map_[z][x - 1] <= 2) //左に棒（壁）がなければ
				{
					Map_[z][x - 1] = 3; //左に棒を倒す。
				}
				else if (Map_[z][x - 1] == 3) //左に棒（壁）があれば
				{
					x = x - 2; //棒を倒さずに、乱数生成をやり直す。
				}
			}
			if (r >= 9 && r <= 12) //rが9から12のとき
			{
				if (Map_[z][x + 1] <= 2) //右に棒（壁）がなければ
				{
					Map_[z][x + 1] = 3; //右に棒を倒す。
				}
				else if (Map_[z][x + 1] == 3) //右に棒（壁）があれば
				{
					x = x - 2; //棒を倒さずに、乱数生成をやり直す。
				}
			}
		}

	}

	int cnt = 0;
	int rx, rz;
	//砂壁の生成
	//壁を砂壁に変更
	while (cnt < 30)
	{
		rx = rand() % (max_width-2) + 1;
		rz = rand() % (max_height-2) + 1;
		if (Map_[rz][rx] < 3)continue;//地面じゃなければやり直す
		Map_[rz][rx] = 4;
		cnt++;
	}
}

//地形生成(初期設定以外の地形を生成　(Map.cpp Line19で設定した以外))
//生成される場所は右上と左下
void Map::Mapparameter(BOOLEAN branch,int num)
{
	int offset_ = 0;
	if (branch)
	{
		for (int z = 0; z <= 16; z++)
		{
			for (int x = 0; x <= 16 - offset_; x++)
			{
				Map_[z][x] = num;
				TerrainMap_[z][x] = num;

			}
			offset_++;
		}
	}
	else
	{
		for (int z = max_height - 1; z >= 7; z--)
		{
			for (int x = max_width - 1; x >= 7 + offset_; x--)
			{
				Map_[z][x] = num;
				TerrainMap_[z][x] = num;
			}
			offset_++;
		}
	}
}

//地形に合わせた速度
float Map::GetMap(float z, float x)
{
	switch (Map_[(int)z][(int)x])
	{
	case TYPE_GRS:return 0.13f; break;//草の時スピード0.13
	case TYPE_SAD:return 0.09f; break;//砂の時スピード0.09
	case TYPE_WTR:return 0.06f; break;//水の時スピード0.06
	default:break;
	}
}

//進む方向
XMVECTOR Map::Dir(DIR dir)
{
	move = g_XMZero;
	switch (dir)
	{
	case UP:move.vecZ = 1; break;
	case RIGHT:move.vecX = 1; break;
	case DOWN:move.vecZ = -1; break;
	case LEFT:move.vecX = -1; break;
	default:break;
	}
	return move;
}

//進路が異なっていた場合変更する
bool Map::IsDir(int isdir)
{
	if (Dir_ != (DIR)isdir)return true; return false;
}

//進路を設定する
void Map::SetDir(int direction)
{
	Dir_ = (DIR)direction;
}
