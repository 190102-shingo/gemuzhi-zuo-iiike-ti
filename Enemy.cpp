#include "Enemy.h"
#include "Map.h"
#include "Player.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "PlayScene.h"
#include "Engine/Camera.h"



//コンストラクタ
Enemy::Enemy(GameObject* parent)
	: GameObject(parent, "Enemy"), EnemyModel_(-1)
{
	//PlaySceneのポインタ
	PlayScene* pScene = dynamic_cast<PlayScene*>(GetParent());
	//Mapのポインタ
	pMap = (Map*)(pScene->pGameMap);
	//Playerのポインタ
	pPlayer = (Player*)(pScene->pGamePlayer);

}

//初期化
void Enemy::Initialize()
{
	//モデルの読み込み
	EnemyModel_ = Model::Load("Model/box.fbx");
	assert(EnemyModel_ >= 0);
	//初期位置
	transform_.position_ = XMVectorSet(pMap->max_width - 1.5, 0, pMap->max_height - 1.5, 0);
	//HPの初期化
	Hp_ = 100;
	jx_ = (int)transform_.position_.vecX;
	jz_ = (int)transform_.position_.vecZ;
	IsPass_ = IsRoot_[(int)transform_.position_.vecZ][(int)transform_.position_.vecX];
	//ゴール地点の設定
	SetGoal();
	//経路探索
	Route_search();
	//経路探索のゴールフラグ
	IsG_ = true;

	Move_ = pMap->Dir(pMap->Dir_);
	
}

//更新
void Enemy::Update()
{
	if (Hp_ <= 0)KillMe();
	/*
		移動
		├今のマスがゴールだった場合(if)
		｜└１秒待機
		│　└次のゴールを決める
		├次のマスに移動していた場合(if)
		│└移動方向を決める
		│　└移動方向が変わった場合(if)
		│　　└向きを変える
		└移動プロセス
		　├移動速度の設定
		　└移動
	*/

	//１秒待機
	if (time_-- > 0)return;


	//ゴール判定
	IsGoal();
	
	//視野
	//if (Eye())
	//{
	//	Gx_ = PlayerPosition.vecX;
	//	Gz_ = PlayerPosition.vecZ;
	//	//経路探索
	//	Route_search();
	//}

	//移動処理
	Move();


	
		
	//キャラクターが次のマスに移動していた場合
	if (IsPass_)
	{
			//現在地点を最短経路から外す
			IsRoot_[(int)transform_.position_.vecZ][(int)transform_.position_.vecX] = false;
			
			//誤差修正
			transform_.position_.vecZ = (int)transform_.position_.vecZ + 0.5f;
			transform_.position_.vecX = (int)transform_.position_.vecX + 0.5f;

			//次の移動方向の策定
			for (int Root_ = 0; Root_ < pMap->DIR_MAX; Root_++)
			{
				//移動方向(次の最短経路)だった場合
				if (IsRoot_[(int)transform_.position_.vecZ + pMap->Search_[Root_][0]][(int)transform_.position_.vecX + pMap->Search_[Root_][1]])
				{
					
					//前回と向きが違った場合
					if (pMap->IsDir(Root_))
					{
						//方向をセット
						pMap->SetDir(Root_);
						//移動方向をリセット
						Move_ = g_XMZero;
						//キャラクターの向きを変更
						transform_.rotate_.vecY = (pMap->Dir_) * 90;
					}
					
				}
			}
	}

}

//描画
void Enemy::Draw()
{
	Model::SetTransform(EnemyModel_, transform_);
	Model::Draw(EnemyModel_);
}
//開放
void Enemy::Release()
{
}

//経路探索(再帰)
void Enemy::Route(int sz_, int sx_)
{
	//キャラが通るポイント
	IsRoot_[sz_][sx_] = 1;
	//チェック済み
	PathMap_[sz_][sx_] = 2;
	//経路探索がゴールについたら終了するフラグ(falseにする)
	if (sx_ == Gx_ && sz_ == Gz_)IsG_ = 0;
	//四方探索
	for (int i = 0; i < 4; i++)
	{
		int startZ = sz_ + pMap->Search_[i][0];
		int startX = sx_ + pMap->Search_[i][1];
		//まだゴールではなくチェックしていなく壁がなければ再帰
		if (IsG_ && PathMap_[startZ][startX] == 0)
			Route(startZ, startX);
	}
	//最短ではない
	if (IsG_)IsRoot_[sz_][sx_] = 0;
}


//ゴール座標の設定
void Enemy::SetGoal()
{
	while (1)
	{
		Gx_ = rand() % (pMap->max_width - 2) + 1;
		Gz_ = rand() % (pMap->max_height - 2) + 1;
		//ゴール地点が壁の場合再設定
		if (pMap->IsWall(Gz_, Gx_))	break; continue;		
	}
}

void Enemy::Route_search()
{
	//経路の初期化
	for (int z = 0; z < pMap->max_height; z++)
	{
		for (int x = 0; x < pMap->max_width; x++)
		{
			if (!pMap->IsWall(z, x))
			{
				PathMap_[z][x] = 1;
			}
			else {
				PathMap_[z][x] = 0;
			}
		}
	}

	//キャラクターが通る経路を初期化
	ZeroMemory(IsRoot_, sizeof(IsRoot_));

	//経路探索
	Route((int)transform_.position_.vecZ, (int)transform_.position_.vecX);
}

//ゴール判定
void Enemy::IsGoal()
{
	for (int i = 0; i < 4; i++)
	{
		if (Move_.vecX == pMap->Search_[i][1] && Move_.vecZ == pMap->Search_[i][0])
		{
			jz_ = (int)transform_.position_.vecZ + Back_ * pMap->Search_[i][0];
			jx_ = (int)transform_.position_.vecX + Back_ * pMap->Search_[i][1];
		}
	}

	//今のマスがゴールだった場合
	if (jz_ == Gz_ && jx_ == Gx_)
	{
		//１秒待機
		time_ = 60;

		//ゴール地点の再設定
		SetGoal();
		//経路探索
		Route_search();
		//ゴール未到達に戻す
		IsG_ = true;

		return;
	}
}

//移動
void Enemy::Move()
{
	//移動ベクトル
	if (Move_.vecX == 1 && Move_.vecZ == 0)IsPass_ = IsRoot_[(int)transform_.position_.vecZ][(int)(transform_.position_.vecX - Back_)];
	else if (Move_.vecX == -1 && Move_.vecZ == 0)IsPass_ = IsRoot_[(int)transform_.position_.vecZ][(int)(transform_.position_.vecX + Back_)];
	else if (Move_.vecX == 0 && Move_.vecZ == 1)IsPass_ = IsRoot_[(int)(transform_.position_.vecZ - Back_)][(int)transform_.position_.vecX];
	else if (Move_.vecX == 0 && Move_.vecZ == -1)IsPass_ = IsRoot_[(int)(transform_.position_.vecZ + Back_)][(int)transform_.position_.vecX];
	else if (Move_.vecX == 0 && Move_.vecZ == 0)IsPass_ = IsRoot_[(int)transform_.position_.vecZ][(int)transform_.position_.vecX];


	//地面の状態に合わせた移動速度の変更
	Speed_ = pMap->GetMap(transform_.position_.vecZ, transform_.position_.vecX);

	//移動方向
	Move_ = pMap->Dir(pMap->Dir_);

	//キャラクターの移動
	transform_.position_ += Move_ * Speed_;
	//キャラの進んだ距離
	LimitSpeed_ += Speed_;
}

bool Enemy::Eye()
{
	//□■□
	//□敵□
	//□□□
	//敵の正面を視野とする
	

	//進行方向を見る
	//	├プレイヤーを見つけた
	//	│	└ゴール地点をプレイヤーに設定
	//	└プレイヤー未発見
	//		└何もしない(ゴール地点に向かって進行)

	PlayerPosition = pPlayer->GetPosition();

	if(Move_.vecX > 0 || Move_.vecZ > 0)
	{
		if(PlayerPosition.vecX < transform_.position_.vecX + Move_.vecX && PlayerPosition.vecX > transform_.position_.vecX &&
			PlayerPosition.vecZ < transform_.position_.vecZ + Move_.vecZ && PlayerPosition.vecZ > transform_.position_.vecZ)	return true;
	}
	else
	{
		if (PlayerPosition.vecX > transform_.position_.vecX + Move_.vecX && PlayerPosition.vecX < transform_.position_.vecX &&
			PlayerPosition.vecZ > transform_.position_.vecZ + Move_.vecZ && PlayerPosition.vecZ < transform_.position_.vecZ)	return true;
	}
	return false;
}















