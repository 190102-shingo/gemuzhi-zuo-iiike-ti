#include "Transform.h"


//コンストラクタ
Transform::Transform(): pParent_(nullptr)
{
	position_ = XMVectorSet(0, 0, 0, 0);
	rotate_ = XMVectorSet(0, 0, 0, 0);
	scale_ = XMVectorSet(1, 1, 1, 0);
	matTranslate_ = XMMatrixIdentity();
	matRotate_ = XMMatrixIdentity();
	matScale_ = XMMatrixIdentity();
}

//デストラクタ
Transform::~Transform()
{
}

//計算
void Transform::Calclation()
{
	//移動行列
	matTranslate_ = XMMatrixTranslation(position_.vecX, position_.vecY, position_.vecZ);

	//回転行列
	XMMATRIX rotateX, rotateY, rotateZ;
	rotateX = XMMatrixRotationX(XMConvertToRadians(rotate_.vecX));
	rotateY = XMMatrixRotationY(XMConvertToRadians(rotate_.vecY));
	rotateZ = XMMatrixRotationZ(XMConvertToRadians(rotate_.vecZ));
	matRotate_ = rotateZ * rotateX * rotateY;

	//拡大縮小
	matScale_ = XMMatrixScaling(scale_.vecX, scale_.vecY, scale_.vecZ);
}

//それぞれ計算したやつを返す
XMMATRIX Transform::GetWorldMatrix() 
{
	Calclation();
	if (pParent_)
	{
		return  matScale_ * matRotate_ * matTranslate_ * pParent_->GetWorldMatrix();
	}

	return  matScale_ * matRotate_ * matTranslate_;
}

