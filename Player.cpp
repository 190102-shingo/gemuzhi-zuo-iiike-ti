#include "Player.h"
#include "Map.h"
#include "Enemy.h"
#include "Item.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "PlayScene.h"
#include "Engine/Camera.h"



//コンストラクタ
Player::Player(GameObject* parent)
	: GameObject(parent, "Player"),PlayModel_(-1)
{
	
}

//初期化
void Player::Initialize()
{
	//モデルの読み込み
	PlayModel_= Model::Load("Model/player_stop.fbx");
	assert(PlayModel_ >= 0);
	//初期位置
	transform_.position_ = XMVectorSet(1.5, 0, 1.5, 0);


	

}

//更新
void Player::Update()
{
	//プレイシーンポインタ
	PlayScene* pScene = dynamic_cast<PlayScene*>(GetParent());
	//Mapのポインタ
	pMap = (Map*)(pScene->pGameMap);
	//Enemyのポインタ
	pEnemy = (Enemy*)(pScene->pGameEnemy);
	//Itemのポインタ
	pItem = (Item*)(pScene->pGameItem);
	
	//カメラの視点、焦点
	Camera::SetPosition(XMVectorSet(transform_.position_.vecX, transform_.position_.vecY + 20 ,transform_.position_.vecZ - 5, 0));
	Camera::SetTarget(XMVectorSet(transform_.position_.vecX, 0, transform_.position_.vecZ, 0));
	
	//動く
	Move();	
	//壁を掘る
	Dig();
	//攻撃
	Attack();
}

//描画
void Player::Draw()
{
	Model::SetTransform(PlayModel_, transform_);
	Model::Draw(PlayModel_);
}
//開放
void Player::Release()
{
}

void Player::Move()
{
	//移動用ベクトル
	move = g_XMZero;

	if (Input::IsKey(DIK_W))
	{
		//向きの初期化
		direction = { 0,0 };
		//上向き
		move.vecZ = direction.y = 1;
	}
	if (Input::IsKey(DIK_S))
	{
		//向きの初期化
		direction = { 0,0 };
		//下向き
		move.vecZ = direction.y = -1;
	}
	if (Input::IsKey(DIK_A))
	{
		//向きの初期化
		direction = { 0,0 };
		//左向き
		move.vecX = direction.x = -1;
	}
	if (Input::IsKey(DIK_D))
	{
		//向きの初期化
		direction = { 0,0 };
		//右向き
		move.vecX = direction.x = 1;
	}

	//地面の状態に合わせた移動速度の変更
	Speed_ = pMap->GetMap(transform_.position_.vecZ, transform_.position_.vecX);

	//壁との当たり判定
	if(pMap->IsWall(transform_.position_.vecZ,transform_.position_.vecX + move.vecX * Speed_))
	transform_.position_.vecX += move.vecX * Speed_;
	if (pMap->IsWall(transform_.position_.vecZ + move.vecZ * Speed_, transform_.position_.vecX))
	transform_.position_.vecZ += move.vecZ * Speed_;

	//moveに入っている値によってキャラの向きを変える
	if (XMVector3Length(move).vecX > 0)
	{
		Direction(move);
	}
	

}

void Player::Direction(XMVECTOR move)
{
	//正規化
	move = XMVector3Normalize(move);
	//デフォルトの向き
	XMVECTOR front = g_XMIdentityR2;
	//内積を求める
	float dot = XMVector3Dot(move, front).vecX;
	//アークコサイン
	float angle = acos(dot);
	//外積を求める
	XMVECTOR cross = XMVector3Cross(front, move);
	//外積がマイナスの場合
	if (cross.vecY < 0)
	{
		angle *= -1;
	}
	//度に変換
	transform_.rotate_.vecY = XMConvertToDegrees(angle);
}

//砂壁を掘る
void Player::Dig()
{
	//左シフトまたは右シフトが押されたら
	if (Input::IsKeyDown(DIK_LSHIFT) || Input::IsKeyDown(DIK_RSHIFT))
	{
		//向いている先が砂壁かどうか
		if (pMap->IsSandWall(transform_.position_.vecZ + direction.y * Speed_, transform_.position_.vecX + direction.x * Speed_))
		{
			//アイテムを入手
			pItem->GetItem();
			//壁をなくしその場に合う地面を表示
			pMap->Map_[(int)transform_.position_.vecZ + direction.y][(int)transform_.position_.vecX + direction.x] =  pMap->TerrainMap_[(int)transform_.position_.vecZ + direction.y][(int)transform_.position_.vecX + direction.x];
		}
	}
}

//攻撃
void Player::Attack()
{
	//エンターが押されたら
	if (Input::IsKeyDown(DIK_RETURN))
	{
		//向いている方向
		//左右の判定
		if (direction.y == 0)
		{
			switch (direction.x)
			{
				//右
			case 1:
				if (transform_.position_.vecX <= pEnemy->GetPosition().vecX && transform_.position_.vecX + direction.x >= pEnemy->GetPosition().vecX)
				{
					Motion_ = true;

				}
				break;
				//左
			case -1:
				if (transform_.position_.vecX >= pEnemy->GetPosition().vecX && transform_.position_.vecX + direction.x <= pEnemy->GetPosition().vecX)
				{
					Motion_ = true;
				}
				break;
			default:
				break;
			}
		}
		//上下の判定
		else if (direction.x == 0)
		{
			switch (direction.y)
			{
				//上
			case 1:
				if (transform_.position_.vecZ <= pEnemy->GetPosition().vecZ && transform_.position_.vecZ + direction.y >= pEnemy->GetPosition().vecZ)
				{
					Motion_ = true;
				}
				break;
				//下
			case -1:
				if (transform_.position_.vecZ >= pEnemy->GetPosition().vecZ && transform_.position_.vecZ + direction.y <= pEnemy->GetPosition().vecZ)
				{
					Motion_ = true;
				}
				break;
			default:
				break;
			}
		}
	}

	//フラグが立っていたら
	if (Motion_)
	{
		//アイテム使用
		if (pItem->UseItem())
			//敵を消す(選択したアイテムが１つ以上所持している場合)
			pEnemy->Hp_ = pItem->DamageCalculation(pEnemy->Hp_);
		//フラグを戻す
		Motion_ = false;
	}
}





