#include "PlayScene.h"
#include "Map.h"
#include "Player.h"
#include "Enemy.h"
#include "Item.h"


//コンストラクタ
PlayScene::PlayScene(GameObject* parent)
	: GameObject(parent, "PlayScene")
{
	
}

//初期化
void PlayScene::Initialize()
{
	

	//マップ表示
	pGameMap = Instantiate<Map>(this);
	//プレイヤー表示
	pGamePlayer = Instantiate<Player>(this);
	//アイコン表示
	pGameItem = Instantiate<Item>(this);

	
	
}

//更新
void PlayScene::Update()
{
	//敵がいなければ
	if (FindObject("Enemy") == nullptr)
	{
		//敵表示
		pGameEnemy = Instantiate<Enemy>(this);
	}
}

//描画
void PlayScene::Draw()
{
	
}

//開放
void PlayScene::Release()
{
}


