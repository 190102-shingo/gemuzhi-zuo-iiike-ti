#pragma once
#include "Engine/GameObject.h"

class Map;
class Player;

//タイトルシーン管理するクラス
class Enemy : public GameObject
{
	//モデル番号
	int EnemyModel_;
	//Mapのポインタ
	Map* pMap;
	//Playerのポインタ
	Player* pPlayer;
	//ゴール
	int Gx_, Gz_,jx_,jz_;
	//経路
	int PathMap_[29][29];
	//経路探索状況(true=ゴール未到達)
	int IsG_ = true;
	
	//移動速度
	float Speed_ = 0.0f;
	//移動距離を取得
	float LimitSpeed_ = 1.0f;
	//キャラの背後を求めるため、中心から背後までの差分
	float Back_ = 0.5f;
	
	//最短経路の記憶(true=道順)
	bool IsRoot_[29][29];
	bool Flag_ = true;
	bool IsPass_ = false;
	XMVECTOR Move_ = g_XMZero;
	
	//停止時間
	int time_ = 0;

	XMVECTOR PlayerPosition = g_XMZero;

public:

	//HP
	int Hp_;
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Enemy(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//経路を決める
	//引数：sx_,sz_ スタート地点
	void Route(int sx_, int sz_);

	//ゴール設定
	void SetGoal();
	
	//経路用配列とフラグ用配列を初期化
	void Route_search();

	//ゴール判定
	void IsGoal();

	//移動
	void Move();

	//視野
	bool Eye();

};