#pragma once
#include "Engine/GameObject.h"

class Map;
class Enemy;
class Item;

//タイトルシーン管理するクラス
class Player : public GameObject
{
	//モデル番号
	int PlayModel_;
	//移動速度
	float Speed_;
	//消すかどうか
	bool Motion_ = false;
	//Mapポインタ
	Map* pMap;
	//Enemyポインタ
	Enemy* pEnemy;
	//Itemポインタ
	Item* pItem;
	//移動
	XMVECTOR move;
	//向き(上下、左右)
	XMINT2 direction;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	Player(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//移動
	void Move();

	//向きを変える
	//引数　方向
	void Direction(XMVECTOR move);

	//壁堀り
	void Dig();

	//攻撃
	void Attack();

};